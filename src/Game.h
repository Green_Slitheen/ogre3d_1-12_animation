#pragma once

#include "Ogre.h"
#include "OgreApplicationContext.h"
#include "OgreInput.h"
#include "OgreRTShaderSystem.h"
#include "OgreApplicationContext.h"
#include "OgreCameraMan.h"


using namespace Ogre;
using namespace OgreBites;

class Game : public ApplicationContext, public InputListener
{
private:
    SceneManager* scnMgr;

    // For animation
    // box entity must be visible in a few methods.
    Entity* box;
    SceneNode* boxNode;

    //Ninja also
    Entity* ninjaEntity;
    SceneNode* ninjaSceneNode;

    // Current animation state - contains relative timeline.
    AnimationState * mBoxAnimationState;
    AnimationState* mNinjaAnimationState;

public:
	Game();
	virtual ~Game();

	void setup();

	void setupCamera();

	void setupBoxMesh();
  void setupNinjaMesh();
  
  void setupNinjaAnimation();
  void setupBoxAnimation();

	void setupLights();

	bool keyPressed(const KeyboardEvent& evt);
	bool mouseMoved(const MouseMotionEvent& evt);

	bool frameStarted (const FrameEvent &evt);
	bool frameEnded(const FrameEvent &evt);

  //This is new!
  bool frameRenderingQueued(const FrameEvent& evt);

};
